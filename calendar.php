<?
    // read Valravn Wiki Spielrunden and make it iCal
    // 04/2024 by D3 https://dnddeutsch.de
    // CC-BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.de
    
    // -------------------------------------------------------------------------
    // PARAM
    // -------------------------------------------------------------------------
    
    // if parameter debug is set, all caches are refreshed or ignored and mime type is changed to plaintext
    $debug = isset($_GET['debug']);
    
    // if parameter archive is set, the archive-page is added
    $archive = isset($_GET['archive']);

    // if parameter google is set, redirect to google calendar
    if(isset($_GET['google'])) {
        if($archive) {
            header("Location: https://calendar.google.com/calendar/embed?src=d1d62f891fe1a503c17f0ed9af117535ee7754662d13364cda840db5c277e553%40group.calendar.google.com&ctz=Europe%2FBerlin");
        } else {
            header("Location: https://calendar.google.com/calendar/embed?src=fb0e934fd3e6db3ef86c2c8b4c7c2b72b801249bcfb39e41f7922f003a95636a%40group.calendar.google.com&ctz=Europe%2FBerlin");
        }
        exit();
    }

    // -------------------------------------------------------------------------
    // CONFIG
    // -------------------------------------------------------------------------
    
    $cache['ical']['file']      = 'cache/valravn-spielrunden'.($archive ? '-archive' : '-current').'.ics';
    $cache['ical']['maxage']    = 60*15; // 15 minutes
    
    $wikibase       = 'https://campaignwiki.org/wiki/Valravn';
    
    // the actual pages with dates
    $pages          = [ $wikibase.'/Spielrunden' ];
    if($archive) {
        $pages[] = $wikibase.'/Spielrunden_Archiv';
    }
    $cache['page']['file']      = 'cache/'.md5(serialize($pages));
    $cache['page']['maxage']    = -1; 
    
    // -------------------------------------------------------------------------
    // MAIN
    // -------------------------------------------------------------------------
    
    if(!$debug) {
        header("Content-type: text/calendar");
        header("Content-Disposition: attachment; filename=valravn-spielrunden".($archive ? '-archive' : '-current').".ics");
    } else {
        header("Content-type: text/plain");

        foreach($cache as $c) {
            unlink($c['file']);
        }        
    }
    
    // is cache younger than 15 minutes?
    // if not then read ics from cache and exit
    if(file_exists($cache['ical']['file']) && filemtime($cache['ical']['file']) > time() - $cache['ical']['maxage']) {
        readfile($cache['ical']['file']);
        exit;
    }

    
    // cache is older than 15 minutes

    // read wiki pages
    $content = '';
    foreach($pages as $page) {
        $ch = curl_init($page.'?action=browse;raw=1');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'User-Agent: ShadowD3rk Calendar (https://shadowdark.dnddeutsch.de/)'
        ]);
        $content .= curl_exec($ch);
        curl_close($ch);
    }

    if($content) {
        file_put_contents($cache['page']['file'], $content);
    } else {
        // couldnt read from remote
        // try cached page
        if(file_exists($cache['page']['file'])) {
            $content = file_get_contents($cache['page']['file']);
        } else {
            // try cached ics
            if(!$debug && file_exists($cache['ical']['file'])) {
                readfile($cache['ical']['file']);
            } else {
                // nothing 
            }
        }
        exit;
    }
    
    // -------------------------------------------------------------------------
    // ICAL head
    // -------------------------------------------------------------------------
    
    // RFC 5545 standard stuff
    // https://icalendar.org/RFC-Specifications/iCalendar-RFC-5545/
    $ics  = "BEGIN:VCALENDAR\r\n";
    $ics .= "VERSION:2.0\r\n";
    $ics .= "PRODID:https://dnddeutsch.de\r\n";
    $ics .= "METHOD:PUBLISH\r\n";
    
    // RFC 7986 stuff
    // https://icalendar.org/RFC-Specifications/iCalendar-RFC-7986/
    // who uses this?
    $ics .= "NAME:Valravn Spielrunden\r\n";
    $ics .= "DESCRIPTION:Valravn ist ein nicht-kommerzielles Rollenspiel-Hobbyprojekt\r\n";
    $ics .= " nach dem Konzept des Offenen Tisches. Der Name wird Wallraun\r\n";
    $ics .= " ausgesprochen.\r\n";
    $ics .= "URL:".$pages[0]."\r\n";
    $ics .= "SOURCE:https://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."\r\n";
    $ics .= " REFRESH-INTERVAL;VALUE=DURATION:P1H\r\n";
    $ics .= "CATEGORIES:PNPDE,TTRPG,OSR,SHADOWDARK\r\n";
    $ics .= "COLOR:black\r\n";
    $ics .= "IMAGE;VALUE=URI;DISPLAY=BADGE;\r\n";
    $ics .= " FMTTYPE=image/png:https://campaignwiki.org/wiki/Valravn/download/Valravn\r\n";
    $ics .= " _Logo.png\r\n";
    $ics .= "CONFERENCE;VALUE=URI;FEATURE=AUDIO,VIDEO;\r\n";
    $ics .= " LABEL=BBB Videoraum:https://bbb.rlp.net/rooms/hel-y9s-m6y-tse/join\r\n";
    $ics .= "LAST-MODIFIED;TZID=Europe/Berlin:".date('Ymd\THis')."\r\n";
    
    // X- are not standardized and should be ignored by clients
    $ics .= "X-PAGE-LAST-MODIFIED;TZID=Europe/Berlin:".date('Ymd\THis', filemtime($cache['page']['file']))."\r\n";
    

    // set up berlin timezone
    $ics .= "BEGIN:VTIMEZONE\r\n";
    $ics .= "TZID:Europe/Berlin\r\n";
    $ics .= "TZURL:http://tzurl.org/zoneinfo-outlook/Europe/Berlin\r\n";
    $ics .= "X-LIC-LOCATION:Europe/Berlin\r\n";
    // CEST
    $ics .= "BEGIN:DAYLIGHT\r\n";
    $ics .= "DTSTART:19700329T020000\r\n";
    $ics .= "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\r\n";
    $ics .= "TZOFFSETFROM:+0100\r\n";
    $ics .= "TZOFFSETTO:+0200\r\n";
    $ics .= "TZNAME:CEST\r\n";
    $ics .= "END:DAYLIGHT\r\n";
    // CET
    $ics .= "BEGIN:STANDARD\r\n";
    $ics .= "DTSTART:19701025T030000\r\n";
    $ics .= "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\r\n";
    $ics .= "TZOFFSETFROM:+0200\r\n";
    $ics .= "TZOFFSETTO:+0100\r\n";
    $ics .= "TZNAME:CET\r\n";
    $ics .= "END:STANDARD\r\n";
    
    $ics .= "END:VTIMEZONE\r\n";


    // -------------------------------------------------------------------------
    // MARKDOWN PARSING
    // -------------------------------------------------------------------------    

    // match a headline with a date, the following headline with some text, and all text up to the next headline
    // then match that textpart against "**Uhrzeit:** HH:MM - HH:MM"
    // everything else goes into DESCRIPTION

    // e.g.:
    // #### Fun at (01.04.2042)
    // ##### (We're going places)
    // (**Uhrzeit:** (23:45) - (23:46)
    // **you're all going to die**
    // **SRSLY**)
    // #### More Fun at 02.04.2042 ...

    // every line starting with min 1 # is matched as the main headline containing a date
    // dateformats DD.MM.YYYY and YYYY-MM-DD are recognised
    // after that skip to next line starting with min 1 # and match it's text after #
    // after that match all text between the last line and the next starting with #

    // caveat:
    // breaks when someone uses another level of subheadings
    
    // translate month names to dd.mm.yyy format
    $monthnames = [
        'Januar'    => '01',
        'Februar'   => '02',
        'März'      => '03',
        'April'     => '04',
        'Mai'       => '05',
        'Juni'      => '06',
        'Juli'      => '07',
        'August'    => '08',
        'September' => '09',
        'Oktober'   => '10',
        'November'  => '11',
        'Dezember'  => '12'
    ];
    foreach($monthnames as $name => $mm) {
        $content = preg_replace('/(\d?\d)\.\s+'.$name.'\s+(\d\d\d\d)/i', '$1.'.$mm.'.$2', $content);
    }

    if(preg_match_all('/[\r\n]#+\s+[^#]*?(\d?\d\.\d\d\.\d\d\d\d|\d\d\d\d-\d\d-\d\d).*?[\r\n]#+\s*(.*?)[\r\n](.*?)(?=[\r\n]#|$)/s', $content, $sets, PREG_SET_ORDER)) {
        foreach($sets as $set) {
            
            $date = date('Y-m-d', strtotime($set[1]));
            $title = trim($set[2]);
            $desc = trim($set[3]);
            
            // time
            $starttime = '00:00:00';
            $endtime = '00:00:00';
            
            // match a line starting with **Uhrzeit: and containing a clocktime
            // timeformats H:MM, HH:MM, H:MM:SS and HH:MM:SS are recognised
            // a second clocktime separated with "-" or "bis" is expected, but optional
            // if a second clocktime is omitted the first clocktime is used for starttime and endtime
            
            if(preg_match('/\*\*\s*Uhrzeit\s*:.*?(\d?\d:\d\d(?:\d\d)?)\s*(?:(?:-|bis)\s*(\d?\d:\d\d(?:\d\d)?))?/', $desc, $m)) {
                $starttime = str_pad(trim($m[1]), 8, ':00');
                if($m[2]) {
                    $endtime = str_pad(trim($m[2]), 8, ':00');
                } else {
                    $endtime = $starttime;
                }
            }

            // concat clocktimes to $date
            $startts = strtotime($date."T".$starttime);
            if(!$startts) {
                // if starttime was not parseable to a date, then default to 00:00:00
                $startts = strtotime($date."T00:00:00"); 
            }
            $startdate = date('Ymd\THis', $startts);
            
            $endts = strtotime($date."T".$endtime);
            if($endts) {
                $enddate = date('Ymd\THis', $endts);
            } else {
                // if endtime was not parseable as a date, then default to 1 hour after $starttime
                $enddate = date('Ymd\THis', $startts + 60*60); 
            }

            // guid            
            $guid = md5($title.$startdate.$enddate).'@shadowdark.dnddeutsch.de';
            
            // desc
            // remove Uhrzeit line from $desc
            $desc = preg_replace('/\*\*\s*Uhrzeit:.*[\r\n]+/', "", $desc);
            
            // outlook understands HTML in X-ALT-DESC as alternative to DESCRIPTION
            // convert markdown to html; the markdown used in the wiki is limited, but maybe this should be improved by using a real markdown parser like parsedown
            // caveat:
            // html lines may be longer than 75 characters wich is against the convention of ical, but wordwraping 
            // them hard at 75 makes outlook misinterpret links and spaces
            $hdesc = $desc;
            $hdesc = preg_replace('/\*\*\*(.*?)\*\*\*/', '<strong><em>$1</em></strong>', $hdesc);
            $hdesc = preg_replace('/\*\*(.*?)\*\*/', '<strong>$1</strong>', $hdesc);
            $hdesc = preg_replace('/\*(.*?)\*/', '<em>$1</em>', $hdesc);
            $hdesc = preg_replace('/\[(.*?)\]\((.*?)\)/', '<a href=\\"'.$wikibase.'/$2\\">$1</a>', $hdesc);
            $hdesc = preg_replace('/\[\[(.*?)\]\]/', '<a href=\\"'.$wikibase.'/$1\\">$1</a>', $hdesc);
            $hdesc = preg_replace('/[\r\n]+/', "<br>\n", $hdesc);
            $hdesc .= '<br><br><a href=\\"https://campaignwiki.org/wiki/Valravn/Spielrunden\\">zu den Valravn-Spielrunden &raquo;</a><br>';
            
            $d = array_map(function($line) { return wordwrap($line, 70, " \r\n "); }, explode("\n", $hdesc));
            $hdesc = join("\\n\r\n ", $d);
            
            // standard DESCRIPTION
            // todo: just strip_tags from $hdesc?
            $desc = preg_replace('/\*\*\*(.*?)\*\*\*/', '$1', $desc);
            $desc = preg_replace('/\*\*(.*?)\*\*/', '$1', $desc);
            $desc = preg_replace('/\*(.*?)\*/', '$1', $desc);
            $desc = preg_replace('/\[(.*?)\]\(.*?\)/', '$1', $desc);
            $desc = preg_replace('/\[\[(.*?)\]\]/', '$1', $desc);
            $desc = preg_replace('/[\r\n]+/', "\n", $desc);
            
            $d = array_map(function($line) { return wordwrap($line, 70, "\r\n ", true); }, array_map('trim', explode("\n", strip_tags($desc))));
            $desc = join("\\n\r\n ", $d);
            
            // title
            $title = wordwrap(trim($title), 70, "\r\n ", true);
            
            // make a VEVENT from it and add it to ical
            $ics .= "BEGIN:VEVENT\r\n";
            $ics .= "UID:$guid\r\n";
            $ics .= "SUMMARY:$title\r\n";
            $ics .= "DESCRIPTION:$desc\\n\r\n https://campaignwiki.org/wiki/Valravn/Spielrunden\r\n";
            $ics .= "X-ALT-DESC;FMTTYPE=text/HTML:<!DOCTYPE HTML PUBLIC \r\n";
            $ics .= " \"-//W3C//DTD HTML 3.2//EN\"><HTML><BODY>\r\n";
            $ics .= " $hdesc\r\n";
            $ics .= " </BODY></HTML>\r\n";
            $ics .= "CLASS:PUBLIC\r\n";
            $ics .= "DTSTART;TZID=Europe/Berlin:".$startdate."\r\n";
            $ics .= "DTSTAMP;TZID=Europe/Berlin:".$startdate."\r\n";
            $ics .= "DTEND;TZID=Europe/Berlin:".$enddate."\r\n";
            $ics .= "END:VEVENT\r\n";
        }
    }
    
    // end ical
    $ics .= "END:VCALENDAR\r\n";

    // output    
    print $ics;
    
    // save to cache
    file_put_contents($cache['ical']['file'], $ics);
?>